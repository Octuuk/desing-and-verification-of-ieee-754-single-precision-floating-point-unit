class myagent extends uvm_agent;
  
  mytransaction pkt;
  mysequencer seqr0;
  mysequence seq0;
  mydriver drv0;
  mymonitor mon0;
  `uvm_component_utils(myagent)
  
  function new(string name="myagent",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    seqr0=mysequencer::type_id::create("seqr0",this);
    seq0=mysequence::type_id::create("seq0");
    drv0=mydriver::type_id::create("drv0",this);
    mon0=mymonitor::type_id::create("mon0",this);
  endfunction
  
  function void connect_phase(uvm_phase phase);
    drv0.seq_item_port.connect(seqr0.seq_item_export);
  endfunction
  
endclass