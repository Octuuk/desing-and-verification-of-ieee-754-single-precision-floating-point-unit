class mydriver extends uvm_driver#(mytransaction);
  mytransaction drvpacket;
  event send;
  event recv;
  virtual axi_stream drvif;
  `uvm_component_utils(mydriver)
  
  function new(string name="mydriver",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    drvpacket=mytransaction::type_id::create("drvpacket");
    uvm_config_db#(virtual axi_stream)::get(this,"","tbif",drvif);
  endfunction
  
  task run_phase(uvm_phase phase);
    //repeat(100000000)begin
    forever begin
      seq_item_port.get_next_item(drvpacket);
      fork 
      	drive(drvpacket);
        getresp();  
      join
      seq_item_port.item_done();
    end
  endtask

  task drive(mytransaction drvpacket);
    // alınan paketlerin sürülmesi lazım.
    drvif.m_axi_tdata = {drvpacket.sign_a,drvpacket.exp_a,drvpacket.mant_a};
    drvif.m_axi_tvalid = 1'b1;
    while(!drvif.m_axi_tready)begin
      @(posedge drvif.axi_aclk); // wait for slave to be ready
      #1;
    end
    //  this means slave ready and transaction can happen
    @(posedge drvif.axi_aclk); // transaction happened first operand has been send
    #1;
    //drvpacket.randomize();// now randomizing for second operand
    drvif.m_axi_tdata={drvpacket.sign_b,drvpacket.exp_b,drvpacket.mant_b};// driving second operand
    //drvif.m_axi_tvalid=1;
    while(!drvif.m_axi_tready)begin
      @(posedge drvif.axi_aclk); // waiting slave for transaction
      #1;
    end
    // slave is ready first posedge data on the line will be transferred
    @(posedge drvif.axi_aclk); // second operand has been send
    #1;
    drvif.m_axi_tdata = drvpacket.opcode;
    while(!drvif.m_axi_tready)begin
      @(posedge drvif.axi_aclk);
      #1;
    end
    @(posedge drvif.axi_aclk); // opcode has been send now we wait for response
    #1;
    drvif.m_axi_tvalid=1'b0;
    drvif.s_axi_tready=1'b1;
    #1;
    -> send;
  endtask
  
  
  task getresp();
    wait(send.triggered);// transferin gerçekleştiği posedge clock anındayız.
    drvif.m_axi_tdata = 32'b0;
    while(!drvif.s_axi_tvalid || !drvif.s_axi_tready )begin
      @(posedge drvif.axi_aclk); // valid from dut not high yet. 
    end
    // drvif.s_axi_ready and drvif.s_axi_valid is high at next clock tran. will comp.
    @(posedge drvif.axi_aclk); // transaction completed
    -> recv;
  endtask
  
endclass
