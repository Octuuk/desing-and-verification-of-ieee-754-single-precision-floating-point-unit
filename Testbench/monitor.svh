class mymonitor extends uvm_monitor;
  `uvm_component_utils(mymonitor)
  mytransaction transpacket;
  uvm_analysis_port #(mytransaction) monap;
  mytransaction operand_a;
  mytransaction operand_b;
  mytransaction opcode;
  reg [31:0] result;
  reg [1:0]  count=0;
  virtual axi_stream monif;
  function new(string name="mymonitor",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    operand_a=mytransaction::type_id::create("operand_a");
    operand_b=mytransaction::type_id::create("operand_b");
    opcode=mytransaction::type_id::create("opcode");
    transpacket=mytransaction::type_id::create("transpacket");
    monap=new("monap",this);
    uvm_config_db#(virtual axi_stream)::get(this,"","tbif",monif);
  endfunction
  
  task run_phase(uvm_phase phase);
    // OPERAND_A
    //repeat(100000000)begin
    forever begin
    while(count < 2'b11)begin
    	@(posedge monif.axi_aclk)
            if(monif.m_axi_tready  && monif.m_axi_tvalid && count<=2'b10)begin
	        case(count)
	          2'b00:begin// get first operand
		    transpacket.sign_a = monif.m_axi_tdata[31]; // 
                    transpacket.exp_a  = monif.m_axi_tdata[30:23];
                    transpacket.mant_a = monif.m_axi_tdata[22:0];
		    $display("operand_a=%0h",monif.m_axi_tdata);
		    count = count + 1'b1;
	          end
	          2'b01:begin
	            transpacket.sign_b = monif.m_axi_tdata[31]; // 
                    transpacket.exp_b  = monif.m_axi_tdata[30:23];
                    transpacket.mant_b = monif.m_axi_tdata[22:0];
		    $display("operand_b=%0h",monif.m_axi_tdata);
		    count = count + 1'b1;
	          end
	          2'b10:begin
		    transpacket.opcode = monif.m_axi_tdata;// 
		    $display("opcode=%0h",monif.m_axi_tdata);
		    count = count + 1'b1;
                  end
	        endcase	
	     end
    end
    while(count!=2'b00)begin
	@(posedge monif.axi_aclk)
	    if(monif.s_axi_tvalid && monif.s_axi_tready && count==2'b11)begin
		transpacket.result = monif.s_axi_tdata;
		count = 2'b00;
		break; // break from while loop
	    end
    end
    transpacket.print();
    monap.write(transpacket);
    end
  endtask
  
  
endclass