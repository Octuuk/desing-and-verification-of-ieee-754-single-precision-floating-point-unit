class myscoreboard extends uvm_scoreboard;
  //mytransaction operand_a;
  //mytransaction operand_b;
  //mytransaction opcode;
  //mytransaction expected;
  //int error = 0;
  //int okay  = 0;
  //int packet = 0;
  //reg [127:0] transpacket;
  `uvm_component_utils(myscoreboard)
  uvm_analysis_imp#(mytransaction,myscoreboard) scbimp;
  
  function new(string name="myscoreboard",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    scbimp=new("scbimp",this);
    //operand_a=mytransaction::type_id::create("operand_a");
    //operand_b=mytransaction::type_id::create("operand_b");
    //opcode=mytransaction::type_id::create("opcode");
    //expected=mytransaction::type_id::create("expected"); 
  endfunction
  
  task write(mytransaction transpacket);
    //packet=packet + 1;
        // operand a
    shortreal operand_a;
    shortreal operand_b;
    int  opcode;
    shortreal result;
    shortreal expected;
    shortreal errormarg=1.0000001; // error margin
    
    reg [24:0] sig_a;
    reg 	   sign_a;
    reg [7:0]  exp_a;
    reg [22:0] mant_a;
    reg 	   imp_a=1'b1;
    
    reg [24:0] sig_b;
    reg 	   sign_b;
    reg [7:0]  exp_b;
    reg [22:0] mant_b;
    reg 	   imp_b=1'b1;
    
    //reg [31:0] opcode;
    
    reg 	   sign_r;
    reg [7:0]  exp_r;
    reg [22:0] mant_r;
    reg [24:0] sig_r;
    // transpaketi parçala 123-> 127
    //$display("NOW AT GOLDEN_REF"); // 10001001000000000000000000000101
    sign_a = transpacket.sign_a;// 1 bit
    exp_a  = transpacket.exp_a;// 8 bit
    mant_a = transpacket.mant_a; //23 bit
    
    sign_b = transpacket.sign_b;// 1 bit
    exp_b  = transpacket.exp_b;//  8 bit
    mant_b = transpacket.mant_b;//  23 bit
    
    opcode = transpacket.opcode; // 32 bit
	
    operand_a = $bitstoshortreal({sign_a, exp_a, mant_a});
    operand_b = $bitstoshortreal({sign_b, exp_b, mant_b});
    result 	  = $bitstoshortreal({transpacket.result});
    //$display("sign_a=%0b mant_a=%8b exp=%23b",sign_a,mant_a,exp_a);
    $display("operand_a=%10.20f operand_b=%10.20f result=%10.20f",operand_a,operand_b,result);

    case(opcode)
      32'd0:begin // toplama
        expected = operand_a + operand_b;
      end
      32'd1:begin// çıkarma
        expected = operand_a - operand_b;
      end
      32'd2:begin //  çarpma
        expected = operand_a * operand_b;
      end
      32'd3:begin
	expected = operand_a / operand_b;
      end
      default:begin
        //ERROR
      end
    endcase
    
    // error margin comparator
    if(expected > result)begin
      if((expected/result) < errormarg)begin
        $display("OKAY result=%10.10f expected=%10.10f",result,expected);
      end
      else begin
        $display("ERROR result=%10.10f expected=%10.10f",result,expected);
        $finish();
      end
    end
    
    else begin
      if((result/expected) < errormarg)begin
        $display("OKAY result=%10.10f expected=%10.10f",result,expected);
      end
      else begin
        $display("ERROR result=%10.10f expected=%10.10f",result,expected);
        $finish();
      end
      
    end
    
    
  endtask
 
  
endclass