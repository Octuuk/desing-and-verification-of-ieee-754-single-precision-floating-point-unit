class mysequence extends uvm_sequence #(mytransaction);
  
  mytransaction packet;
  
  `uvm_object_utils(mysequence)
  
  function new(string name="mysequence");
    super.new(name);
  endfunction
  
  task body();
    packet=mytransaction::type_id::create("packet");
    repeat(10000000) begin
      start_item(packet);
      packet.randomize();
      finish_item(packet);
    end
    
  endtask
  
endclass