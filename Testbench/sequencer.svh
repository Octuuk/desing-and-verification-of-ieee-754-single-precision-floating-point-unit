class mysequencer extends uvm_sequencer#(mytransaction);
  
  `uvm_component_utils(mysequencer)
  
  function new(string name="mysequencer",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
  endfunction
  
endclass