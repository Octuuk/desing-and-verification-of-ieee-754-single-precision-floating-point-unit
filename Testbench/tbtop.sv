// Code your testbench here
// or browse Examples
`include "package.svh"
`include "uvm_macros.svh"
module tbtop();
  import uvm_pkg::*;
  import mypackage::*;
  // global signals 
  
  axi_stream tbif();
  
  Design dut(.axi_aclk(tbif.axi_aclk),
             .axi_arstn(tbif.axi_arstn),
             // Testbench ----> DUT
             .m_axi_tdata(tbif.m_axi_tdata),  // tb  ---> dut
             .m_axi_tvalid(tbif.m_axi_tvalid),// tb  ---> dut
             .m_axi_tready(tbif.m_axi_tready),// dut ---> tb
             //DUT --------->TB
             .s_axi_tdata(tbif.s_axi_tdata),  // dut ----> tb
             .s_axi_tvalid(tbif.s_axi_tvalid),// dut ----> tb
             .s_axi_tready(tbif.s_axi_tready) // tb  ----> dut
            );
  initial begin
    tbif.axi_arstn = 1;
    //tbif.m_axi_tvalid=1'b1;
    tbif.axi_aclk = 0;
    uvm_config_db#(virtual axi_stream)::set(null,"*","tbif",tbif);
    forever #5 tbif.axi_aclk = ~tbif.axi_aclk;
  end
  
  initial begin
    run_test();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,tbtop); 
  end
  
endmodule