class mytest extends uvm_test;
  myenviroment env0;
  `uvm_component_utils(mytest)
  function new(string name="mytest",uvm_component parent=null);
    super.new(name,parent);
  endfunction
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    env0=myenviroment::type_id::create("env0",this);
  endfunction
  
  task run_phase(uvm_phase phase);
    phase.raise_objection(this);
    env0.agt0.seq0.start(env0.agt0.seqr0);
    phase.drop_objection(this);
    //$display("------------------------OVERALLL-----------------------");
    //$display("packet=%0d okay=%0d error=%0d",env0.scb0.packet,env0.scb0.okay,env0.scb0.error);
  endtask
  
endclass