class mytransaction extends uvm_sequence_item;
  
  rand logic 	    sign_a;
  rand logic [7:0]  exp_a;
  rand logic [22:0] mant_a;
  
  rand logic 		sign_b;
  rand logic [7:0]  exp_b;
  rand logic [22:0] mant_b;
  
  rand logic [31:0] opcode;
       logic [31:0] result;
  
  constraint exp_a_c {exp_a inside {[80:180]};}
  constraint exp_b_c {exp_b inside {[80:180]};}
  
  //constraint mant_a_c {mant_a inside {[0:20]};}
  //constraint mant_b_c {mant_b inside {[0:20]};}
  
  constraint opcode_c {opcode <= 32'd3;}
  
  `uvm_object_utils_begin(mytransaction)
  // operand a
  `uvm_field_int(sign_a,UVM_DEFAULT)
  `uvm_field_int(exp_a,UVM_DEFAULT)
  `uvm_field_int(mant_a,UVM_DEFAULT)
  // operand b
  `uvm_field_int(sign_b,UVM_DEFAULT)
  `uvm_field_int(exp_b,UVM_DEFAULT)
  `uvm_field_int(mant_b,UVM_DEFAULT)
  // opcode
  `uvm_field_int(opcode,UVM_DEFAULT)
  // result
  `uvm_field_int(result,UVM_DEFAULT)
  `uvm_object_utils_end
  
  
  
  function new(string name="mytransaction");
    super.new(name);
  endfunction 
  
  
endclass