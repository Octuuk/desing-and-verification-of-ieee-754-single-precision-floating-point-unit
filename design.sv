// Code your design here
interface axi_stream;

  logic        axi_aclk;
  logic        axi_arstn;
  // input wires
  logic [31:0] m_axi_tdata;
  logic        m_axi_tvalid;
  //input wire        m_axi_tlast,
  logic       m_axi_tready;
  // output section
  logic [31:0] s_axi_tdata;
  logic        s_axi_tvalid;
  logic       s_axi_tready;
  //output reg        s_axi_tlast

  
endinterface


module Design(
// global signals 
input wire        axi_aclk,
input wire        axi_arstn,
// input wires
input wire [31:0] m_axi_tdata,
input wire        m_axi_tvalid,
//input wire        m_axi_tlast,
output wire        m_axi_tready,
// output section
output wire [31:0] s_axi_tdata,
output wire        s_axi_tvalid,
input  wire       s_axi_tready
//output reg        s_axi_tlast
    );
    
parameter ADD=2'b00,SUBS=2'b01,MULTP=2'b10,DIVD=2'b11;
parameter IDLE=3'b000,OP_A_OK=3'b001,OP_B_OK=3'b010,OPCODE_OK=3'b011,CALC=3'b100,REST=3'b101;
parameter NULL=3'b000,ALGN=3'b001,SUM=3'b010,NORM=3'b011,FORMT=3'b100;
reg [2:0] state=IDLE;
reg [2:0]  calc_state=NULL;
// operand a 
reg [31:0] op_a=0;
reg [31:0] op_b=0;
reg [2:0] opcode=0;
// result 
reg [31:0] result_fin;

// internal
wire       calc_n;
reg        calc_as=0;
reg        calc_mltp=0;
reg        calc_divd=0;
reg        done=0;
reg        decimal=0;
// submodule connections 
wire [31:0] result;
// adder sub output
wire [27:0] sig_r_as;
wire [7:0]  exp_r_as;
wire        sign_r_as;
wire        done_as;
// multp output 
wire [27:0] sig_r_mltp;
wire [7:0]  exp_r_mltp;
wire        sign_r_mltp;
wire        done_mltp;

// divider output
wire [27:0] sig_r_divd;
wire [7:0]  exp_r_divd;
wire        sign_r_divd;
wire        done_divd;

// decider output
wire [27:0] sig_r_n;
wire [7:0]  exp_r_n;
wire        sign_r_n;
wire        done_n;

wire        norm_ok;

assign sig_r_n  = (calc_as)?(sig_r_as):((calc_mltp)?sig_r_mltp:((calc_divd)?sig_r_divd:23'b0));// decider decides which module output goes to norm block
assign exp_r_n  = (calc_as)?(exp_r_as):((calc_mltp)?exp_r_mltp:((calc_divd)?exp_r_divd:8'b0));
assign sign_r_n = (calc_as)?(sign_r_as):((calc_mltp)?sign_r_mltp:((calc_divd)?sign_r_divd:1'b0));
assign done_n   = (calc_as)?(done_as):((calc_mltp)?done_mltp:((calc_divd)?done_divd:1'b0));

Adder_V1 ADDSUB(.operand_a(op_a),
                .operand_b(op_b),
                .opcode(opcode),
                .calc(calc_as),
                .sig_r_n(sig_r_as),  // output from submodule
                .exp_r_n(exp_r_as),  // output from submodule
                .sign_r_n(sign_r_as),// output from submodule
                .done(done_as)       // output from submodule
);

MULTP Multp(.operand_a(op_a),// input
            .operand_b(op_b),// input
            //.opcode(opcode),// input
            .calc(calc_mltp),// input 
            .sig_r_n(sig_r_mltp), // output 
            .exp_r_n(exp_r_mltp), // output
            .sign_r_n(sign_r_mltp), //  output 
            .done(done_mltp)   // output
);

DIVIDER DIVID(.axi_aclk(axi_aclk),
	      .operand_a(op_a),
              .operand_b(op_b),
              .calc(calc_divd),
              .sig_r_n(sig_r_divd),
              .exp_r_n(exp_r_divd),
              .sign_r_n(sign_r_divd),
              .done(done_divd)    
);


Normalizer NOMR(.sig_r_n(sig_r_n), // output from decider,input to NORM BLOCK
                .exp_r_n(exp_r_n),
                .sign_r_n(sign_r_n),
                .done(done_n),
                //.calc(calc_n),//  input from parent module
                .result(result),
                .norm_ok(norm_ok)
);
// base input to submodule converter


assign s_axi_tdata = (done_n)?result:32'b0;
assign s_axi_tvalid = norm_ok;
assign m_axi_tready = (state==CALC)?1'b0:1'b1;


// config block. in this block system gets operands and opcodes.in addition it triggers calculation block.And wait until calculation is done.When this happens it returns back to ?dle state
always @(posedge axi_aclk)begin
    if(!axi_arstn)begin
        state <= IDLE;
        // doldururuz.
    
    end
    else begin
        if(state==IDLE && m_axi_tready && m_axi_tvalid)begin
            op_a <= m_axi_tdata;
            state  <= OP_A_OK;
        end
        else if (state==OP_A_OK && m_axi_tready && m_axi_tvalid)begin
            op_b <= m_axi_tdata;
            state <= OP_B_OK;
        end
        else if(state==OP_B_OK && m_axi_tready && m_axi_tvalid)begin
            state <= CALC;
            case(m_axi_tdata)
                ADD:begin
                    opcode <= 3'b000; // add operation
                    calc_as   <= 1'b1;
                end
                SUBS:begin
                    opcode <= 3'b001; // sub operation
                    calc_as   <= 1'b1;
                end
                MULTP:begin
                    opcode <= 3'b010;
                    calc_mltp   <= 1'b1;
                end
                DIVD:begin
                    opcode    <= 3'b011;
                    calc_divd <= 1'b1;
                    //calc   <= 1'b1;
                end
            endcase
        end
        else begin
            if( (state == CALC) && norm_ok && (s_axi_tvalid && s_axi_tready))begin
                state <= IDLE;
                result_fin <= result;
                calc_as   <= 1'b0;
                calc_mltp <= 1'b0;
                calc_divd <= 1'b0;
            end
            else begin
                state <= state;
            end
        end
    end// resetten sonraki block ends
end
endmodule



module Adder_V1(
input  wire [31:0] operand_a,
input  wire [31:0] operand_b,
input  wire [2:0]  opcode,
input  wire        calc,
output wire [27:0] sig_r_n,
output wire [7:0]  exp_r_n,
output wire        sign_r_n,
output reg        done
    );
reg [27:0] sig_a;
reg [27:0] sig_b;
reg [27:0] sig_r;    
reg [7:0]  exp_r;
reg        sign_r;
reg        eq_done;

assign sig_r_n  = (done)?(sig_r):28'b0;
assign exp_r_n  = (done)?(exp_r):8'b0;
assign sign_r_n = (done)?(sign_r):1'b0;
     
always@(*) begin
    if(calc==1'b1)begin
        sig_a = {2'b01,operand_a[22:0],3'b000};
        sig_b = {2'b01,operand_b[22:0],3'b000};
        if(operand_a[30:23] > operand_b[30:23])begin  // exponent a bigger than exponent b that means we need to shift mantissa b to right until exponents become equal
           sig_b = sig_b >> (operand_a[30:23] - operand_b[30:23]);
           exp_r = operand_a[30:23];
           eq_done = 1'b1;
        end
        else if(operand_b[30:23] > operand_a[30:23])begin
           sig_a = sig_a >> (operand_b[30:23] - operand_a[30:23]);
           exp_r = operand_b[30:23];
           eq_done = 1'b1;
        end
        else begin // exponents are equal. That means we dont have to equalize them.
           eq_done = 1'b1;
	       exp_r = operand_a[30:23];
        end

        if (eq_done)begin
            if(opcode == 3'b000)begin //  opcode==1'b0 means add
                if(!(operand_a[31] ^ operand_b[31]))begin // (+) + (+) or (-) + (-) 
                    sign_r  = operand_a[31]; 
                    sig_r   = sig_a + sig_b;
                    //exp_r   = operand_a[30:23];// we already equalized the exponents of the operands 
                end
                else begin
                    if(sig_a > sig_b)begin// (-)+(+)
                        sign_r = operand_a[31];
                        sig_r  = sig_a - sig_b;
                    end
                    else if(sig_b >= sig_a)begin // (+)+(-)
                        sign_r = operand_b[31];
                        sig_r  = sig_b - sig_a;
                    end
                    else begin
                // no op
                    end
                end
            end 
            else if(opcode==3'b001)begin
                if((operand_a[31] ^ operand_b[31]))begin // (-) - (+) or (+) - (-)
                    sign_r = operand_a[31];
                    sig_r  = sig_a + sig_b;
            //exp_r <= exp_a;
            //calc_state <= NORM;
                end
                else begin
                    if(sig_a > sig_b)begin // (-) - (-) or (+) - (+)
                        sign_r = operand_a[31];
                        sig_r  = sig_a - sig_b;
                    end
                    else if(sig_b >= sig_a)begin//(-) - (-) or (+) - (+)
                        sign_r  = ~operand_b[31];
                        sig_r   = sig_b - sig_a;     
                    end
                    else begin
                // no op
                    end
                end
            end  // substract block ends  
            else begin
            // no op
            end
            done = 1'b1;
        end
    end // if(calc==1'b1) blo?u  
    else begin
        eq_done = 1'b0;
        done    = 1'b0;
    end   
end    
endmodule


module Normalizer(
input wire [27:0] sig_r_n,
input wire [7:0]  exp_r_n,
input wire        sign_r_n,
input wire        done,
//input wire        calc,
output wire [31:0] result,
output wire       norm_ok
    );
reg [27:0] sig_r;
reg [7:0]  exp_r;
reg 	   sign_r;
reg        norm;
assign norm_ok = norm;
assign result = (norm)?({sign_r,exp_r,sig_r[25:3]}):32'b0;
    
always @(*)begin
  if(done==1'b0)begin
    norm = 1'b0;
  end
  else begin
    casex(sig_r_n)
        28'b1x_xxxx_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = (sig_r_n[3])?( (sig_r_n >> 1'b1) + 4'b1_000):(sig_r_n >> 1'b1);
            exp_r      = exp_r_n + 1'b1;
        end
        28'bx1_xxxx_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
           sig_r       = (sig_r_n[2])?(sig_r_n + 4'b1_000):sig_r_n;
	   exp_r       = exp_r_n ;
        end
        28'bxx_1xxx_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
           sig_r       = (sig_r_n[1])?((sig_r_n << 1'b1) + 4'b1_000):(sig_r_n << 1'b1) ;
           exp_r       = exp_r_n - 1;
        end
        28'bxx_x1xx_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r       = (sig_r_n[0])?((sig_r_n << 2) + 4'b1_000):(sig_r_n << 2);
            exp_r       = exp_r_n - 2;
        end
        28'bxx_xx1x_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n <<  3;
            exp_r      = exp_r_n - 3;
        end
        28'bxx_xxx1_xxxx_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 4;
            exp_r      = exp_r_n - 4;
        end
        28'bxx_xxxx_1xxx_xxxx_xxxx_xxxx_xxx_xxx:begin
           sig_r       = sig_r_n << 5;
           exp_r       = exp_r_n - 5;
        end
        28'bxx_xxxx_x1xx_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 6;
            exp_r      = exp_r_n - 6;
        end
        28'bxx_xxxx_xx1x_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n <<  7;
            exp_r      = exp_r_n - 7;
        end
        28'bxx_xxxx_xxx1_xxxx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 8;
            exp_r      = exp_r_n - 8;
        end
        28'bxx_xxxx_xxxx_1xxx_xxxx_xxxx_xxx_xxx:begin
           sig_r       = sig_r_n << 9;
           exp_r       = exp_r_n - 9;
        end
        28'bxx_xxxx_xxxx_x1xx_xxxx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 10;
            exp_r      = exp_r_n - 10;
        end
        28'bxx_xxxx_xxxx_xx1x_xxxx_xxxx_xxx_xxx:begin
            sig_r       = sig_r_n <<  11;
            exp_r       = exp_r_n - 11;
        end
        28'bxx_xxxx_xxxx_xxx1_xxxx_xxxx_xxx_xxx:begin
            sig_r       = sig_r_n << 12;
            exp_r       = exp_r_n - 12;
        end
        28'bxx_xxxx_xxxx_xxxx_1xxx_xxxx_xxx_xxx:begin
           sig_r       = sig_r_n << 13;
           exp_r       = exp_r_n - 13;
        end
        28'bxx_xxxx_xxxx_xxxx_x1xx_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 14;
            exp_r      = exp_r_n - 14;
        end
        28'bxx_xxxx_xxxx_xxxx_xx1x_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 15;
            exp_r      = exp_r_n - 15;
        end
        28'bxx_xxxx_xxxx_xxxx_xxx1_xxxx_xxx_xxx:begin
            sig_r      = sig_r_n << 16;
            exp_r      = exp_r_n - 16;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_1xxx_xxx_xxx:begin
           sig_r      = sig_r_n << 17;
           exp_r      = exp_r_n - 17; 
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_x1xx_xxx_xxx:begin
            sig_r      = sig_r_n << 18;
            exp_r      = exp_r_n - 18;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xx1x_xxx_xxx:begin
            sig_r      = sig_r_n << 19;
            exp_r      = exp_r_n - 19;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxx1_xxx_xxx:begin
            sig_r      = sig_r_n << 20;
            exp_r      = exp_r_n - 20;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_1xx_xxx:begin
           sig_r       = sig_r_n << 21;
           exp_r       = exp_r_n - 21;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_x1x_xxx:begin
            sig_r       = sig_r_n << 22;
            exp_r       = exp_r_n - 22;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_xx1_xxx:begin
            sig_r      = sig_r_n <<  23;
            exp_r      = exp_r_n - 23;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx_1xx:begin
            sig_r      = sig_r_n << 24;
            exp_r      = exp_r_n - 24;
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx_x1x:begin
           sig_r      = sig_r_n << 25;
           exp_r      = exp_r_n - 25; 
        end
        28'bxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxx_xx1:begin
            sig_r      = sig_r_n << 26;
            exp_r      = exp_r_n - 26;
        end
        default:begin
            // errorr
        end                                               
    endcase
    sign_r = sign_r_n;
    norm = 1'b1;
  end
end 
  
endmodule

module MULTP(
input wire [31:0] operand_a,
input wire [31:0] operand_b,
//input wire [2:0] opcode,
input wire       calc,
output wire [27:0] sig_r_n,
output wire [7:0] exp_r_n,
output wire       sign_r_n,
output wire       done    
);

reg [53:0] multp;
reg [27:0] sig_a;

reg [27:0] sig_b;

reg [27:0] sig_r;
reg [7:0]  exp_r;
reg        sign_r;
reg        multp_ok;

assign sig_r_n  = (multp_ok)?(sig_r):28'b0;
assign exp_r_n  = (multp_ok)?(exp_r):8'b0;
assign sign_r_n = (multp_ok)?(sign_r):1'b0;
assign done     =  multp_ok;
 
always @(*)begin
    if(calc==1'b1)begin
        sig_a  = {2'b01,operand_a[22:0],3'b000};
        sig_b  = {2'b01,operand_b[22:0],3'b000};
        multp  = sig_a * sig_b; // product of mantissas stored in multp_temp[47:0]
        exp_r  = (operand_a[30:23] + operand_b[30:23]) - 8'd127;
        sign_r = (operand_a[31]) ^ (operand_b[31]); //
        sig_r  = multp[53:26];
        multp_ok = 1'b1;
    end
    else begin
        multp_ok=1'b0;
    end
end
endmodule


module DIVIDER(
input wire        axi_aclk,
input wire [31:0] operand_a,
input wire [31:0] operand_b,
input wire        calc,
output wire [27:0] sig_r_n,
output wire [7:0]  exp_r_n,
output wire        sign_r_n,
output wire        done    
);

reg [27:0] sig_r;
reg [7:0]  exp_r;
reg        sign_r;
// 
reg [27:0] sig_a;
reg [7:0]  exp_a;
reg        sign_a;

reg [27:0] sig_b;
reg [7:0]  exp_b;
reg        sign_b;

reg decimal=1'b0;
reg start=1'b0;
reg [4:0] count=5'b0;
reg       done_n=1'b0;

assign sig_r_n  = (done_n)?sig_r:28'b0;
assign exp_r_n  = (done_n)?exp_r:8'b0;
assign sign_r_n = (done_n)?sign_r:1'b0;
assign done = done_n;

always @(posedge axi_aclk)begin
    if(!start && calc==1'b1)begin
        sig_a <= {2'b01,operand_a[22:0],3'b000};
        sig_b <= {2'b01,operand_b[22:0],3'b000};
        exp_a <= operand_a[30:23];
        exp_b <= operand_b[30:23];
        start <= 1'b1;
    end
    else if(start && calc)begin
        if( (sig_a > sig_b) && ~decimal)begin // this means left side of the point needs to determined
            sig_r <= {2'b01,26'b0}; // 01.0000_0000_0000_0000_0000_000_000
            exp_r  <= exp_a - exp_b + 8'd127;
            sign_r <= operand_a[31] ^ operand_b[31];
            sig_a <= sig_a - sig_b; // 0.101011...
            decimal <= 1'b1; // decimal side is done.Now we can calculate mantissa only
        end
        else if( (sig_b > sig_a) && ~decimal)begin
            sig_r <= {2'b00,26'b0}; // 00.0000_0000_0000_0000_0000_000_000
            exp_r  <= exp_a - exp_b + 8'd127;
            sign_r <= operand_a[31] ^ operand_b[31];
            sig_a <= sig_a << 1'b1; //
            decimal <= 1'b1;
        end
        else if( (sig_a > sig_b) && decimal)begin// decimal side is completed now mantissa
            sig_r[25:0] <= {sig_r[24:0],1'b1}; // xx.1xxx
            sig_a <= sig_a - sig_b;
            count <= count + 1'b1;
            done_n <= ( (count + 1'b1) == 5'd26 )?1'b1:1'b0; // if count eq26 this means we searched all of the bits.now sig_r is completed
        end
        else if( (sig_b > sig_a) && decimal )begin
            //sig_r[25:0] <= {sig_r[24:0],1'b0};
            sig_a <= sig_a << 1'b1;
            if((sig_a << 1'b1) < sig_b)begin
                sig_r[25:0] <= {sig_r[24:0],1'b0};
                count <= count + 1'b1;
                done_n <= ( (count+1'b1) == 5'd26 )?1'b1:1'b0;
            end
        end    
    end
    else begin
        count   <= 5'b0;
        start   <= 1'b0;
        decimal <= 1'b0;
        done_n  <= 1'b0; 
    end
end    
endmodule